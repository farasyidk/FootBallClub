package rasyidk.fa.footballclub

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.jetbrains.anko.*
import rasyidk.fa.footballclub.anko_layout.SecondActivityUI

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        SecondActivityUI().setContentView(this)

        val nama = intent.getStringExtra("name")
        val desc = intent.getStringExtra("desc")
        val img = intent.getIntExtra("img", 0)

        val imgDtlFootBall = findViewById<ImageView>(R.id.imgDtlFootBall)
        val txtDtlNama = findViewById<TextView>(R.id.txtDtlNama)
        val txtDtlDesc = findViewById<TextView>(R.id.txtDtlDesc)

        Glide.with(this).load(img).into(imgDtlFootBall)
        txtDtlNama.text = nama
        txtDtlDesc.text = desc

    }
}