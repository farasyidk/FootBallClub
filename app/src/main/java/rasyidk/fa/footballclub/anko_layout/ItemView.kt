package rasyidk.fa.footballclub.anko_layout

import android.support.v4.widget.TextViewCompat
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import org.jetbrains.anko.*
import rasyidk.fa.footballclub.R
import org.jetbrains.anko.cardview.v7.cardView

class ItemView : AnkoComponent<ViewGroup> {

    override fun createView(ui: AnkoContext<ViewGroup>) = with(ui) {
        verticalLayout {
            lparams(width = matchParent, height = wrapContent)

            cardView {

                verticalLayout {
                    lparams(width = matchParent, height = wrapContent)
                    padding = dip(10)

                    imageView {
                        id = R.id.imgFootBall

                    }.lparams(width = dip(50), height = dip(50))

                    textView {
                        id = R.id.txtNama

                        TextViewCompat.setTextAppearance(this, R.style.Base_TextAppearance_AppCompat_Body2)
                    }.lparams(width = matchParent, height = wrapContent) {
                        marginStart = dip(10)
                        gravity = Gravity.CENTER_VERTICAL
                    }
                }.orientation = LinearLayout.HORIZONTAL

            }.lparams(width = matchParent, height = matchParent) {
                margin = dip(5)
            }

        }
    }

}